# third-step-project 



## @eugene.black91
- [ ] Created Modal classes
- [ ] Created Form classes
- [ ] Created Card logic
- [ ] Created Fields Classes
- [ ] Maintained Visit classes

##  @symetime
- [ ] Created Visit classes
- [ ] Created ShowMore option
- [ ] Created Utilities option
- [ ] Maintained Card logic
- [ ] Maintained Design


## @bogdan01072003
- [ ] Created Login, Logout
- [ ] Created Request class
- [ ] Created Filter functions
- [ ] Created Render methods
- [ ] Maintained Card logic
- [ ] Maintained Visit classes