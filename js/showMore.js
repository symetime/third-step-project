import Modal from "./Modal.js";
import {Requests} from "./Requests.js";
import { URL } from "./constants.js";
import {TherapistTemplate, DantistTemplate, CardiologistTemplate} from "./showTemplates.js"

async function  showMoreHanlder(event) {
    const cardToShow = event.target.closest(".visitCard__container");
    
    const cardId = cardToShow.getAttribute('data-card-id');
    const getUserData = await Requests.GET(`${URL}${cardId}`);
    $('#myModal').modal('show');
    populateModal(getUserData);


}

function populateModal(object) {
    
    var modalBody = document.getElementById("modalBody");
    if(modalBody) {
        modalBody.innerHTML = "";
        if (object.doctor == "Дантист") {
            modalBody.innerHTML = DantistTemplate(object);
            
        } else if (object.doctor == "Терапевт") {
            modalBody.innerHTML = TherapistTemplate(object);
        } else if (object.doctor == "Кардиолог") {
            modalBody.innerHTML = CardiologistTemplate(object);
        } else {
            console.log("no such doctor")
        }
        
        // Loop through object fields and append to modal body
        // for (var key in object) {
        // if (object.hasOwnProperty(key)) {
        //     var fieldRow = document.createElement("div");
        //     fieldRow.className = "row";
        //     fieldRow.innerHTML = "<div class='col-md-6'><strong>" + key + "</strong></div><div class='col-md-6'>" + object[key] + "</div>";
        //     modalBody.appendChild(fieldRow);
        //     }
        // }
    } else {
        console.log("no modalBody");
    }
}

function closeTab() {
    $('#myModal').modal('hide');
}
document.querySelector('.clsBtn').addEventListener('click', closeTab);

export {showMoreHanlder}