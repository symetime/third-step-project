function CardiologistTemplate(obj) {

    let element = `<div class="row">
        <div class="col-md-6"><strong>Цель визита</strong></div>
        <div class="col-md-6">${obj.purpose}</div>
    </div>
    <div class="row">
        <div class="col-md-6"><strong>Описание Болезни</strong></div>
        <div class="col-md-6">${obj.description}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>ФИО</strong></div>
        <div class="col-md-6">${obj.fio}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Приоритет</strong></div>
        <div class="col-md-6">${obj.priority}</div></div>
    <div class="row">
    <div class="col-md-6"><strong>Доктор</strong></div>
        <div class="col-md-6">${obj.doctor}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Нормальное давление</strong></div>
        <div class="col-md-6">${obj.normPressure}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Масса Тела</strong></div>
        <div class="col-md-6">${obj.bodyMass}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Предыдущие заболевания</strong></div>
        <div class="col-md-6">${obj.prevDiseases}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Возраст</strong></div>
        <div class="col-md-6">${obj.age}</div></div>
    `
    return element;
}

function DantistTemplate(obj) {
    let element = `<div class="row">
    <div class="col-md-6"><strong>Цель визита</strong></div>
    <div class="col-md-6">${obj.purpose}</div>
    </div>
    <div class="row">
        <div class="col-md-6"><strong>Краткое описание визита</strong></div>
        <div class="col-md-6">${obj.description}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>ФИО</strong></div>
        <div class="col-md-6">${obj.fio}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Приоритет</strong></div>
        <div class="col-md-6">${obj.priority}</div></div>
    <div class="row">
    <div class="col-md-6"><strong>Доктор</strong></div>
        <div class="col-md-6">${obj.doctor}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Дата последнего визита</strong></div>
        <div class="col-md-6">${obj.lastVisit}</div></div>
    <div class="row">`

    return element;
}

function TherapistTemplate(obj) {
    let element = `<div class="row">
    <div class="col-md-6"><strong>Доктор</strong></div>
    <div class="col-md-6">${obj.doctor}</div>
    </div>
    <div class="row">
        <div class="col-md-6"><strong>Краткое описание</strong></div>
        <div class="col-md-6">${obj.description}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>ФИО</strong></div>
        <div class="col-md-6">${obj.fio}</div></div>
    <div class="row">
        <div class="col-md-6"><strong>Приоритет</strong></div>
        <div class="col-md-6">${obj.priority}</div></div>
    <div class="row">
    <div class="col-md-6"><strong>Возраст</strong></div>
        <div class="col-md-6">${obj.age}</div></div>
`

    return element;
}

export {TherapistTemplate, DantistTemplate, CardiologistTemplate}